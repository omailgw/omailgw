function setMessage(string){
    const alertDiv = $("#alert");
    alertDiv.removeClass().addClass("alert alert-info");
    alertDiv.empty();
    if (!string || string == 'undefined : 0'){
        string= 'FAIL';
    }
    debug(string)
    switch (string){
      case 'EMPTY' :
        alertDiv.hide();
      break;
      case "LOAD":
        alertDiv.html('<span data-i18n-key="loading"></span> ');
        alertDiv.show();
      break;
      case "CREATE":
        alertDiv.removeClass().addClass("alert alert-success");
        alertDiv.html('<span data-i18n-key="creating"></span> ');
        alertDiv.show();
      break;
      case "ADD":
        alertDiv.removeClass().addClass("alert alert-success");
        alertDiv.html('<span data-i18n-key="adding"></span> ');
        alertDiv.show();
      break;
      case "DELETE":
        alertDiv.removeClass().addClass("alert alert-success");
        alertDiv.html('<span data-i18n-key="deleting"></span> ');
        alertDiv.show();
      break;
      case "SUCCESS":
        alertDiv.removeClass().addClass("alert alert-success");
        alertDiv.html('<span data-i18n-key="success"></span> ');
        alertDiv.show();
      break;
      case 'FAIL':
        alertDiv.removeClass().addClass("alert alert-danger");
        alertDiv.html('<span data-i18n-key="failConnect"></span> ');
        alertDiv.show();
      break;
      default:
        alertDiv.removeClass().addClass("alert alert-danger");
        alertDiv.html(string);
        alertDiv.show();
      break;
    }
    translateData();
}
