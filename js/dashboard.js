import { LogStat } from "./api_connector.js";
;
import showRightInfos from './commun.js';

Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

showRightInfos();

async function updateDashboard(dateStart, dateEnd, limit = 10, type = []) {
  let getLogStatTry = await LogStat(dateStart, dateEnd, limit, type);
  if (getLogStatTry[0]) {
    var data = getLogStatTry[1];
  } else {
    setMessage(getLogStatTry[2].status + " : " + getLogStatTry[1]);
  }
  // Calc %
  if (data.countOutError != 0) {
    var percentOutError=Math.round(100*data.countOutError/data.countTotal);
  } else {
    var percentOutError=0;
  }
  if (data.countInError != 0) {
    var percentInError=Math.round(100*data.countInError/data.countTotal);
  } else {
    var percentInError=0;
  }
  var countAllError=data.countOutError+data.countInError;
  var percentAllError=Math.round(percentInError+percentOutError);
  var percentAllEmail=Math.round(100-percentOutError-percentInError);
  // Remove class total-stat card
  // $(".total-stat").removeClass (function (index, className) {
  //  return (className.match (/(^|\s)bg-\S+/g) || []).join(' ');
  //});

  // Print to card
  $('#countTotal').text(data.countTotal);
  $('.countTotal .card-body').click(function() {
    location.href="#log-table?frmDate="+moment.unix(dateStart).format("YYYY-MM-DD")+"&toDate="+moment.unix(dateEnd).format("YYYY-MM-DD");
  });
  $('.total-stat.countTotal').addClass("bg-primary");
  $('#countInError').text(data.countInError);
  $('.countInError .card-body').click(function() {
    location.href="#log-table?type=errorIn&frmDate="+moment.unix(dateStart).format("YYYY-MM-DD")+"&toDate="+moment.unix(dateEnd).format("YYYY-MM-DD");
  });
  if (percentInError > settings.dashboard_percentInError_danger) {
    $('.total-stat.countInError').addClass("bg-danger");
  } else if (percentInError > settings.dashboard_percentInError_warning) {
    $('.total-stat.countInError').addClass("bg-warning");
  } else {
    $('.total-stat.countInError').addClass("bg-success");
  }
  $('#countOutError').text(data.countOutError);
  $('.countOutError .card-body').click(function() {
    location.href="#log-table?type=errorOut&frmDate="+moment.unix(dateStart).format("YYYY-MM-DD")+"&toDate="+moment.unix(dateEnd).format("YYYY-MM-DD");
  });
  if (percentOutError > settings.dashboard_percentOutError_danger) {
    $('.total-stat.countOutError').addClass("bg-danger");
  } else if (percentOutError > settings.dashboard_percentOutError_warning) {
    $('.total-stat.countOutError').addClass("bg-warning");
  } else {
    $('.total-stat.countOutError').addClass("bg-success");
  }
  $('#percentAllError').text(percentAllError+'%');
  if (percentAllError > settings.dashboard_percentAllError_danger) {
    $('.total-stat.percentAllError').addClass("bg-danger");
  } else if (percentAllError > settings.dashboard_percentAllError_warning) {
    $('.total-stat.percentAllError').addClass("bg-warning");
  } else {
    $('.total-stat.percentAllError').addClass("bg-success");
  }

  // Print table
  if (data.topOutDomain) {
    for (let i = 0; i < data.topOutDomain.length; i++) {
      $('#topOutDomain tr:last').after('<tr><td>'+data.topOutDomain[i].nb+'</td><td>'+data.topOutDomain[i].dom+'</td></tr>');
    }
  }
  if (data.topOutDomainError) {
    for (let i = 0; i < data.topOutDomainError.length; i++) {
      $('#topOutDomainError tr:last').after('<tr><td>'+data.topOutDomainError[i].nb+'</td><td>'+data.topOutDomainError[i].dom+'</td></tr>');
    }
  }

  // Build Char
  var labelsGraphCountByDay = [];
  var dataGraphCountByDay = [];
  var dataGraphCountErreurInByDay = [];
  var dataGraphCountErreurOutByDay = [];
  for (const [key, value] of Object.entries(data.getByDay)) {
    labelsGraphCountByDay.push(key);
    dataGraphCountByDay.push(value.total);
    dataGraphCountErreurInByDay.push(value.errorIn);
    dataGraphCountErreurOutByDay.push(value.errorOut);
  }
  var ctx = document.getElementById("logsGraphArea");
  var myLineChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: labelsGraphCountByDay,
      datasets: [{
        label: "Total e-mail",
        lineTension: 0.3,
        backgroundColor: "rgb(13, 110, 253, 0.05)",
        borderColor: "#0d6efd",
        pointRadius: 3,
        data:  dataGraphCountByDay,
      },{
        label: "E-mail rejeté à l'arrivé",
        lineTension: 0.8,
        backgroundColor: "rgb(0, 0, 0, 0.05)",
        borderColor: "#000",
        pointRadius: 3,
        pointBorderWidth: 2,
        data:  dataGraphCountErreurInByDay,
      },{
        label: "E-mail rejeté à la sortie",
        lineTension: 0.8,
        backgroundColor: "rgb(220, 53, 69, 0.05)",
        borderColor: "#dc3545",
        pointRadius: 3,
        pointBorderWidth: 2,
        data:  dataGraphCountErreurOutByDay,
      }],

    },
    options: {
      layout: {
        padding: {
          left: 10,
          right: 25,
          top: 25,
          bottom: 0
        }
      },
      scales: {
        xAxes: [{
          time: {
            unit: 'date'
          },
          gridLines: {
            display: false,
            drawBorder: false
          },
          ticks: {
            maxTicksLimit: 7
          }
        }],
        yAxes: [{
          ticks: {
            maxTicksLimit: 5,
            padding: 10,
          },
          gridLines: {
            color: "rgb(234, 236, 244)",
            zeroLineColor: "rgb(234, 236, 244)",
            drawBorder: false,
            borderDash: [2],
            zeroLineBorderDash: [2]
          }
        }],
      },
      legend: {
        display: false
      },
      tooltips: {
        backgroundColor: "rgb(255,255,255)",
        bodyFontColor: "#858796",
        titleMarginBottom: 10,
        titleFontColor: '#6e707e',
        titleFontSize: 14,
        borderColor: '#dddfeb',
        borderWidth: 1,
        xPadding: 15,
        yPadding: 15,
        displayColors: false,
        intersect: false,
        mode: 'index',
        caretPadding: 10
      }
    }
  });



  var ctx = document.getElementById("PercentPieChart");
  new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["E-mail délivré (%)", "E-mail rejeté à l'arrivé (%)", "E-mail rejeté en sortie (%)"],
      datasets: [{
        data: [percentAllEmail, percentInError, percentOutError],
        backgroundColor: ['#0d6efd', '#000', '#dc3545'],
        hoverBackgroundColor: ['rgba(78, 115, 223, 0.05)', 'rgba(0, 0, 0, 0.05)', 'rgb(220, 53, 69, 0.05)'],
        hoverBorderColor: "rgba(234, 236, 244, 1)",
      }],
    },
    options: {
      tooltips: {
        backgroundColor: "rgb(255,255,255)",
        bodyFontColor: "#858796",
        borderColor: '#dddfeb',
        borderWidth: 1,
        xPadding: 15,
        yPadding: 15,
        displayColors: false,
        caretPadding: 10,
      },
      cutoutPercentage: 80,
    },
  });
}


// Document ready :
jQuery(function() {
  const locationHash = window.location.hash;
  const queryString = locationHash.split('?');
  debug("query : "+queryString[1]);
  const urlParams = new URLSearchParams(queryString[1]);
  debug("days : " + urlParams.get('days'));
  var days = urlParams.get('days');
  // Select days form if isset in get params
  if (days) {
    $('#days option[value='+days+']').prop('selected', true);
  }
  // Si on change les jours
  $('#days').on('change', function() {
    window.location.href = './#dashboard?days=' + this.value;
  });
  var timestamp_now = Math.floor(Date.now() / 1000);
  var timestamp_start = Math.floor(timestamp_now - $('#days').find(":selected").val() * 86400);

  updateDashboard(timestamp_start, timestamp_now, 10, ["countTotal", "countOutError", "countInError", "topOutDomain", "topOutDomainError", "getByDay"])
});
