import { ChangePassword, Logout } from "./api_connector.js";
;
import showRightInfos from './commun.js';

// Update datas to show
showRightInfos();
$('#loadData').hide();
setMessage('EMPTY');

// Use the submit
document.querySelector('#changePasswordForm').addEventListener('submit', async (event) => {
  event.preventDefault();
  setMessage('LOAD');

  let currentPassword = document.querySelector('#inputCurrentPassword').value;
  let newPassword = document.querySelector('#inputNewPassword').value;
  let newPasswordConfirm = document.querySelector('#inputNewPasswordConfirm').value;

  // Clear previous & show messages
  let errorMessage = "";
  // Check current pwd is ok
  if (currentPassword === "") {
    errorMessage += "<span data-i18n-key='passwordNNullcurrent'>Please enter your current password. </span>";
  }
  if (errorMessage == "" && currentPassword.length < settings.password_min_length) {
    errorMessage += "<span data-i18n-key='invalidCreds'>Invalid credentials</span>";
  }
  // Check new pwd fits
  if (newPassword.length < settings.password_min_length) {
    errorMessage += "<span data-i18n-key='passwordlength'>Please enter a new password of at least</span> " + settings.password_min_length + "<span data-i18n-key='chars'> characters.</span>";
  }
  if (newPassword != newPasswordConfirm) {
    errorMessage += "<span data-i18n-key='passwordRepeat'>Please repeat your new password.</span>";
  }
  if (errorMessage != "") {
    setMessage(errorMessage);
    return;
  }
  // Handle the API response
  let changePasswordTry = await ChangePassword(currentPassword, newPassword);
  if (changePasswordTry[0]) {
    setMessage('EMPTY');
    currentPassword = "";
    newPassword = "";
    newPasswordConfirm = "";
    setMessage("SUCCESS");
    Logout();
    window.location.replace("login.html");
  } else {
    setMessage(changePasswordTry[2].status + " : " + changePasswordTry[1]);
  }
});
