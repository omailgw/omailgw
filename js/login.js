import { Login, IsLoggedIn } from "./api_connector.js";
;

// Check if already loggedIn
if (IsLoggedIn()) {
  window.location.replace("index.html");
}
setMessage('EMPTY');

// use login div as button
document.getElementById("loginButton").addEventListener("click", async (event) => {
  event.preventDefault();
  setMessage('LOAD');
  // References html elements
  const email = document.getElementById("inputEmail").value;
  const password = document.getElementById("inputPassword").value;
  const rememberMe = document.getElementById("inputRememberPassword").checked;

  // Clear previous & show messages
  let errorMessage = '';
  if (email === "") {
    errorMessage += "<span data-i18n-key='mailNNull'>Please enter an email. </span>";
  }else if (!settings.emailRegex.test(email)){
    errorMessage += "<span data-i18n-key='mailValid'>Please enter a valid email adress. </span>";
  }
  if (password === "") {
    errorMessage += "<span data-i18n-key='passwordNNull'>Please enter a password. </span>";
  }
  if (errorMessage == "" && password.length < settings.password_min_length) {
    errorMessage += "<span data-i18n-key='invalidCreds'>Invalid credentials. Please try again.</span>";
  }
  if (errorMessage != "") {
    setMessage(errorMessage);
    return;
  };
  // Handle the API response
  // wait for the login function from api_connector.js
  let loginTry = await Login(email, password, rememberMe);
  setMessage('EMPTY');
  if (loginTry[0]) { // first element in array = true => success
    window.location.replace("index.html");
    localStorage.setItem("userName", email);
  } else {
    setMessage(loginTry[2].status + " : " + loginTry[1]);
  }


});
