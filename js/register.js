import { IsLoggedIn, Register } from "./api_connector.js";
;

// Check if already loggedIn
if (IsLoggedIn()) {
  window.location.replace("index.html");
}

// Use the submit
document.querySelector('#registerForm').addEventListener('submit', async (event) => {
  event.preventDefault();
  setMessage('LOAD');

  const email = document.querySelector('#inputEmail').value;
  const password = document.querySelector('#inputPassword').value;
  const passwordConfirm = document.querySelector('#inputPasswordConfirm').value;
  let errorMessage = "";

  // Clear previous & show messages
  errorMessage = "";
  if (password.length < settings.password_min_length) {
    errorMessage += "<span data-i18n-key='passwordlength'></span>Please enter a password of at least " + settings.password_min_length + "<span data-i18n-key='chars'> characters.</span>";
  }
  if (email.length === "") {
    errorMessage += "<span data-i18n-key='mailNNull'>Please enter an email. </span>";
  } else if (!settings.emailRegex.test(email)) {
    errorMessage += "<span data-i18n-key='mailValid'>Please enter a valid email adress. </span>";
  }
  if (password != passwordConfirm) {
    errorMessage += "<span data-i18n-key='passwordRepeat'>Please repeat your new password.</span>";
  }
  if (errorMessage != "") {
    setMessage(errorMessage);
    return;
  };

  // Handle the API response
  let registerTry = await Register(email, password);
  if (registerTry[0]) {
    setMessage('SUCCESS');
    email = "";
    password = "";
    passwordConfirm = "";
    window.location.replace("login.html");
  } else {
    setMessage(registerTry[2].status + " : " + registerTry[1]);
  }
});
