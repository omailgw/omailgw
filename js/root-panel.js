;
import showRightInfos from './commun.js';

showRightInfos();

// Update datas to show
jQuery(function() {
  UpdateAdminList();
});

// Select html objects
const formNew = document.querySelector('#admin-form');
const adminTable = document.getElementById("admin-list");

async function UpdateAdminList() {
  setMessage('LOAD');
  // Ask infos from API : Reports Settings
  let getadminTry = await GetAdminList();
  if (getadminTry[0]) {
    setMessage('EMPTY');
    let AdminList = getadminTry[1];
    // First foreach adds rows
    adminTable.innerHTML = "";
    for (let Admin of AdminList){
      adminTable.innerHTML +=
        `
        <tr id="id${Admin.id}">
          <td>${Admin.username}</td>
          <td>${Admin.role}</td>
          <td><button type='button' class='btn btn-info info-button' data-i18n-key="modify">Modify</button></td>
          <td><button type='button' class='btn btn-danger delete-button' data-i18n-key="delete">Delete</button></td>
        </tr>
        `;
    };
    //second foreach adds listeners
    for (const row of adminTable.querySelectorAll('tr')) {
      // Add event listener to the delete button
      row.querySelector('.delete-button').addEventListener('click', () => {
        const idCell = row.id;
        debug(idCell);
        Change(idCell);
        row.remove(); // Remove the row from the table
      });
      row.querySelector('.delete-button').addEventListener('click', () => {
        const usernameCell = row.querySelector('td:first-child');
        const username = usernameCell.textContent.trim();
        Delete(username);
        row.remove(); // Remove the row from the table
      });
    }
  } else {
    setMessage(getadminTry[2].status +' : '+ getadminTry[1]);
  }
}

// Handle deletion action
async function Delete(email) {
  setMessage('DELETE');
  // Handle the API response
  let deleteTry = await DeleteAdmin(email);
  if (deleteTry[0]) {
    setMessage('SUCCESS')
    UpdateAdminList();
  } else {
    setMessage(deleteTry[2].status +' : '+ deleteTry[1]);
  }
};


// Handle new Admin  'Form' (not a <form> as it is on multiple <th> but behaves like one thanks to multiple listeners)
formNew.addEventListener('submit', async (event) => {
  event.preventDefault();
  setMessage('ADD');

  const email = formNew.querySelector('input[name="newusername"]').value;
  const description = formNew.querySelector('input[name="newdescribe"]').value;

  // Clear previous & show messages
  let errorMessage = "";
  if (email === "") {
    errorMessage += "<span data-i18n-key='mailNNull'>Please enter an email. </span>"
  }
  if (errorMessage != "") {
    setMessage(errorMessage);
    return;
  };

  // Handle the API response
  let addAuthTry = await AddAdmin(email, description);
  if (addAuthTry[0]) {
    setMessage("SUCCESS");
    UpdateAdminList();
  } else {
    setMessage(addAuthTry[2].status + " : " + addAuthTry[1]);
  }
});

// Listen for "enter" keydown events on the input fields
formNew.querySelector('input[name="newusername"]').addEventListener('keydown', (event) => {
  if (event.keyCode === 13) {
    event.preventDefault();
    formNew.dispatchEvent(new Event('submit'));
  }
});

// Listen for "click" events on the "Add" button
formNew.querySelector('button[type="submit"]').addEventListener('click', (event) => {
  event.preventDefault();
  formNew.dispatchEvent(new Event('submit'));
});

async function Change(id) {
  setMessage('LOAD');
  debug("change id="+id)
  //retrouver les infos du parent
  row = document.querySelector("#id"+id);
  email = row.querySelector('td:first-child').textContent;

  // Handle the API response
  let getLogpermissionsTry = await GetLogs(userid);
  if (getLogpermissionsTry[0]) {
    setMessage('SUCCESS');
    UpdateAdminList();
  } else {
    setMessage(getLogpermissionsTry[2].status + " : " + getLogpermissionsTry[1]);
  }

  // Ouvrir une fenêtre popup avec les paramètres de l'administrateur
  const popup = window.open('', '', 'width=400,height=400');
  popup.document.write(`
    <html>
      <body>
        <h2><span data-i18n-key="modifySettings">Modifier les paramètres de </span>${email}</h2>
        <form>
          <label for="role" data-i18n-key="role">Role</label>
          <input type="text" id="role" name="role" value="${admin.role}">
          <br><br>
          <label for="email" data-i18n-key="email">Email</label>
          <input type="email" id="email" name="email" value="${admin.email}">
          <br><br>
          <button type="submit"  data-i18n-key="apply">Enregistrer</button>
        </form>
      </body>
    </html>
  `);
  // Gérer l'événement de soumission du formulaire
  popup.document.querySelector('form').addEventListener('submit', (event) => {
    event.preventDefault();
    // Mettre à jour les paramètres de l'administrateur
    admin.role = popup.document.getElementById('role').value;
    admin.email = popup.document.getElementById('email').value;
    // Fermer la fenêtre popup
    popup.close();
  });
}

// ******************* API-connector

export async function AddAdmin(email, description){
  /*
  debug("Asking API to add user : " + email + " described as=" + describe);
  debug("api url = " + settings.api_url + " following route : " + settings.api_adminadd);

  let headers = settings.headersList;
  headers.Authorization = "Bearer " + localStorage.getItem("token");

  const bodyContent = JSON.stringify({
    "username": username,
    "description": describe
  });

  try {
    const response = await $.ajax({
      url: settings.api_url + settings.api_smtpadd,
      method: "POST",
      data: bodyContent,
      headers: headers
    });

    debug("api_connector : added SMTP auth successfully ! ");
    return [true];
  } catch (error) {
    debug("api_connector : adding SMTP auth failed : " + error.statusText + "\". Response's xhr object :");
    debug(error);
    let msgFromAPI = "Could not connect to server.";
    if (error != undefined && error.responseJSON != undefined && error.responseJSON.message != undefined) {
      msgFromAPI = error.responseJSON.message;
    }
    debug(msgFromAPI);
    return [false, msgFromAPI, error];
  }
  */
}

export async function DeleteAdmin(email){
}

export async function GetAdminList() {
  debug("Asking API to get users list");
  debug("api url = " + settings.api_url + " following route : " + settings.api_adminlist);

  let headers = settings.headersList;
  headers.Authorization = "Bearer " + localStorage.getItem("token");

  try {
    const response = await $.ajax({
      url: settings.api_url + settings.api_adminlist,
      method: "GET",
      headers: headers
    });
    debug("api_connector : got users list successfully !");
    $('#loadData').hide();
    return [true, response];
  } catch (error) {
    debug("api_connector : getting SMTP list failed : " + error.statusText + "\". Response's xhr object :");
    debug(error);
    let msgFromAPI = "Could not connect to server.";
    if (error != undefined && error.responseJSON != undefined && error.responseJSON.message != undefined) {
      msgFromAPI = error.responseJSON.message;
    }
    debug(msgFromAPI);
    return [false, msgFromAPI, error];
  }
}

export async function Getlogs(id){
  debug("Asking API to get log permissions for : " + id);
  debug("api url = " + settings.api_url + " following route : " + settings.api_userlist);

  let headers = settings.headersList;
  headers.Authorization = "Bearer " + localStorage.getItem("token");

  try {
    const response = await $.ajax({
      url: settings.api_url + settings.api_userlist +"/"+id+"/log-permissions",
      method: "GET",
      headers: headers
    });

    debug("api_connector : got log permissions successfully ! ");
    return [true, response];
  } catch (error) {
    debug("api_connector : getting log permissions failed : " + error.statusText + "\". Response's xhr object :");
    debug(error);
    let msgFromAPI = "Could not connect to server.";
    if (error != undefined && error.responseJSON != undefined && error.responseJSON.message != undefined) {
      msgFromAPI = error.responseJSON.message;
    }
    debug(msgFromAPI);
    return [false, msgFromAPI, error];
  }
}
